//
//  ViewController.swift
//  xibFileExample
//
//  Created by Juan Calvo on 9/28/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import UIKit
import MBProgressHUD


extension UIViewController {
    func showloader() {
        MBProgressHUD.showAdded(to: view, animated: true)
    }
    
    func hideLoader() {
        MBProgressHUD.hide(for: view, animated: true)
    }
}
