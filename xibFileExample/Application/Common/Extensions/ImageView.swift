//
//  ImageView.swift
//  xibFileExample
//
//  Created by Juan Calvo on 7/22/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import UIKit

extension UIImageView {
    func setWebImage(url:String) {
        self.sd_setImage(with: URL(string: url)!, placeholderImage: #imageLiteral(resourceName: "placeHolder"))
    }
    
}
