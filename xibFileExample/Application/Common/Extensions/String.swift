//
//  String.swift
//  xibFileExample
//
//  Created by Juan Calvo on 7/11/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import Foundation
extension String {
    public func trim() -> String {
        return trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
}
