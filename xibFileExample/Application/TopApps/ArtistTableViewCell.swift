//
//  ArtistTableViewCell.swift
//  xibFileExample
//
//  Created by Juan Calvo on 7/22/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import UIKit

class ArtistTableViewCell: UITableViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var urlLabel: UILabel!
    
    func configure(artist:Artist) {
        nameLabel.text = artist.name
        urlLabel.text = artist.url
    }
}
