//
//  TopAppTableViewController.swift
//  xibFileExample
//
//  Created by Juan Calvo on 7/22/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import UIKit

class TopAppTableViewController: UITableViewController {
    let connection = ConnectionManager.shared
    var data:[Any] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "AppTableViewCell", bundle: nil), forCellReuseIdentifier: "appCell")
        tableView.register(UINib(nibName: "ArtistTableViewCell", bundle: nil), forCellReuseIdentifier: "artistCell")
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(TopAppTableViewController.reloadData),
                                               name: NSNotification.Name(Constants.reloadTableNotificationName),
                                               object: nil)
        showloader()
        connection.getData { (apps, artist) in
            self.data.append(contentsOf: apps)
            self.data.append(contentsOf: artist)
            NotificationCenter.default.post(name: NSNotification.Name(Constants.reloadTableNotificationName), object: nil)
        }
    }
    
    @objc private func reloadData() {
        tableView.reloadData()
        hideLoader()
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (data[indexPath.row] as? App) != nil {
            return 250
        } else {
            return 223
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let app = data[indexPath.row] as? App {
            let cell = tableView.dequeueReusableCell(withIdentifier: "appCell", for: indexPath) as! AppTableViewCell
            cell.configure(app: app)
            return cell
        } else if let artist = data[indexPath.row] as? Artist {
            let cell = tableView.dequeueReusableCell(withIdentifier: "artistCell", for: indexPath)  as! ArtistTableViewCell
            cell.configure(artist: artist)
            return cell
        } else {
            return UITableViewCell()
        }
    }
}
