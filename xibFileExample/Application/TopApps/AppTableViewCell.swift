//
//  AppTableViewCell.swift
//  xibFileExample
//
//  Created by Juan Calvo on 7/22/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import UIKit

class AppTableViewCell: UITableViewCell {
    @IBOutlet var artworkImage: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    
    func configure(app:App) {
        titleLabel.text = app.name
        artworkImage.setWebImage(url: app.artworkUrl)
    }
}
