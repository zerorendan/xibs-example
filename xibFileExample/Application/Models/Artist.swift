//
//  Artist.swift
//  xibFileExample
//
//  Created by Juan Calvo on 7/11/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import UIKit
import SwiftyJSON

class Artist: NSObject {
    var id: String
    var name: String
    var url: String
    var appId: String
    
    init(artistDetail:JSON) {
        id = artistDetail[Constants.ApiKeys.Artist.id].string ?? Constants.defaultTextApi
        name = artistDetail[Constants.ApiKeys.Artist.name].string ?? Constants.defaultTextApi
        url = artistDetail[Constants.ApiKeys.Artist.url].string ?? Constants.defaultTextApi
        appId = artistDetail[Constants.ApiKeys.Artist.appId].string ?? Constants.defaultTextApi
    }
}
